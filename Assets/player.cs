﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    public AudioClip[] audio_clip;
    public AudioSource audio_source;

    // camera
    public new Camera camera;

    public float zoom_delta = 0.25f;
    public float rotate_delta = 90f;
    public float rotate_multiplier = 0.25f;
    public float camera_angle;
    public float target_camera_angle;
    public float epsilon = 0.1f;

    private float camera_rotate_cooldown_timer = 0.0f;
    private float camera_rotate_cooldown = 0.15f;

    // helpers
    Vector3 y_axis = new Vector3(0, 1, 0);

    float clip(float value, float min, float max)
    {
        return value = (value < min) ? min : ((value > max) ? max : value);
    }

    void Start()
    {
        camera_angle = target_camera_angle = camera.transform.rotation.y;
    }

    void PlaySound(int index, float volume)
    {
        if (audio_source.isPlaying)
            volume *= 0.5f;
        audio_source.PlayOneShot(audio_clip[index], volume);
    }

    void HandleInput()
    {
        var deltaMouseWheel = Input.GetAxis("Mouse ScrollWheel");
        // zoom
        if (deltaMouseWheel > 0f)
        {
            camera.orthographicSize -= zoom_delta;
        }
        else if (deltaMouseWheel < 0f)
        {
            camera.orthographicSize += zoom_delta;
        }

        // rotate
        if (Input.GetKeyDown(KeyCode.Z))
        {
            camera_rotate_cooldown_timer = camera_rotate_cooldown;
            target_camera_angle += rotate_delta;
            PlaySound(0, 0.5f);
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            camera_rotate_cooldown_timer = camera_rotate_cooldown;
            target_camera_angle -= rotate_delta;
            PlaySound(0, 0.5f);
        }
    }

    void CheckWalls()
    {
        float characterCenterY = 0.5f;
        float spread = 2.0f;

        Vector3 adjustedCharacterPosition = transform.position + new Vector3(0, characterCenterY, 0);
        Vector3 cameraDirection = (camera.transform.position - adjustedCharacterPosition).normalized;
        Vector3 cameraDirectionFlat = cameraDirection; cameraDirectionFlat.y = 0;
        cameraDirectionFlat.Normalize();
        Vector3 p1 = adjustedCharacterPosition + spread * (Quaternion.Euler(0, 45, 0) * cameraDirectionFlat);
        Vector3 p2 = adjustedCharacterPosition + spread * (Quaternion.Euler(0, -45, 0) * cameraDirectionFlat);

        Ray wallTestCRay = new Ray(adjustedCharacterPosition, cameraDirection);
        Ray wallTestLRay = new Ray(p1, cameraDirection);
        Ray wallTestRRay = new Ray(p2, cameraDirection);

        RaycastHit[] hitLInfo;
        RaycastHit[] hitRInfo;
        RaycastHit[] hitCInfo;

        hitCInfo = Physics.RaycastAll(wallTestCRay, 100.0F);
        hitLInfo = Physics.RaycastAll(wallTestLRay, 100.0F);
        hitRInfo = Physics.RaycastAll(wallTestRRay, 100.0F);

        var walls = GameObject.FindGameObjectsWithTag("canBeInvisible");
        for (var i = 0; i < walls.Length; ++i)
        {
            var update_visibility = walls[i].GetComponent<updateVisibility>();
            if (update_visibility)
            {
                bool hit = false;
                var j = 0;
                for (j = 0; j < hitCInfo.Length && !hit; ++j)
                {
                    if (walls[i].name == hitCInfo[j].collider.gameObject.name)
                    {
                        update_visibility.setTargetVisibility(0);
                        hit = true;
                        break;
                    }
                }
                for (j = 0; j < hitLInfo.Length && !hit; ++j)
                {
                    if (walls[i].name == hitLInfo[j].collider.gameObject.name)
                    {
                        update_visibility.setTargetVisibility(0);
                        hit = true;
                        break;
                    }
                }
                for (j = 0; j < hitRInfo.Length && !hit; ++j)
                {
                    if (walls[i].name == hitRInfo[j].collider.gameObject.name)
                    {
                        update_visibility.setTargetVisibility(0);
                        hit = true;
                        break;
                    }
                }
                if (!hit)
                    update_visibility.setTargetVisibility(1);
            }
        }
    }

    void Update()
    {
        camera.transform.position = transform.position;

        if (camera_rotate_cooldown_timer > 0)
        {
            camera_rotate_cooldown_timer -= Time.deltaTime;
        }
        else
        {
            HandleInput();
        }

        float rotate_value = (target_camera_angle - camera_angle) * rotate_multiplier;

        if (rotate_value != 0)
        {
            if (Mathf.Abs(rotate_value) <= epsilon)
            {
                rotate_value = target_camera_angle - camera_angle;
                camera_angle = target_camera_angle;
            }
            else
            {
                camera_angle += rotate_value;
            }
        }

        camera.orthographicSize = clip(camera.orthographicSize, 2, 8);
        camera.transform.RotateAround(transform.position, y_axis, rotate_value);
        camera.transform.Translate(0, 1, -20);

        CheckWalls();
    }
}
